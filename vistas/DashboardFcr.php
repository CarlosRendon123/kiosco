 <!-- Content Header (Page header) -->
 <div class="content-header">
     <div class="container-fluid">
         <div class="row mb-2">
             <div class="col-sm-6">
                 <h1 class="m-0">Tablero FCR</h1>
                 <p id="actualizacion"></p>
             </div><!-- /.col -->
             <div class="col-sm-6">
                 <ol class="breadcrumb float-sm-right">
                     <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                     <li class="breadcrumb-item active">FCR</li>
                 </ol>
             </div><!-- /.col -->
         </div><!-- /.row -->
     </div><!-- /.container-fluid -->
 </div>
 <!-- /.content-header -->

 <!-- Main content -->
 <div class="content">
     <div class="container-fluid">
         <div class="row">
             <div class="col-md-12">
                 <!-- AREA CHART -->
                 <div class="card card-primary">
                     <div class="card-header">
                         <h3 class="card-title">Encuesta NPS: ¿Tu requerimiento fue solucionado en nuestro Kiosko de Autoservicio?</h3>

                         <div class="card-tools">
                             <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                 <i class="fas fa-minus"></i>
                             </button>
                             <button type="button" class="btn btn-tool" data-card-widget="remove">
                                 <i class="fas fa-times"></i>
                             </button>
                         </div>
                     </div>
                     <div class="card-body">
                         <div class="chart">
                             <canvas id="areaChart1" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                         </div>
                     </div>
                     <!-- /.card-body -->
                 </div>
                 <!-- /.card -->

             </div>
             <!-- /.col (LEFT) -->


         </div>
         <!-- /.row -->

         <div class="row">
             <div class="col-md-6">
                 <!-- LINE CHART -->
                 <div class="card card-info">
                     <div class="card-header">
                         <h3 class="card-title">Proveedores Si</h3>

                         <div class="card-tools">
                             <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                 <i class="fas fa-minus"></i>
                             </button>
                             <button type="button" class="btn btn-tool" data-card-widget="remove">
                                 <i class="fas fa-times"></i>
                             </button>
                         </div>
                     </div>
                     <div class="card-body">
                         <div class="chart">
                             <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                         </div>
                     </div>
                     <!-- /.card-body -->
                 </div>
                 <!-- /.card -->


             </div>
             <!-- /.col (RIGHT) -->


             <div class="col-md-6">
                 <!-- LINE CHART -->
                 <div class="card card-danger">
                     <div class="card-header">
                         <h3 class="card-title">Proveedores No </h3>

                         <div class="card-tools">
                             <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                 <i class="fas fa-minus"></i>
                             </button>
                             <button type="button" class="btn btn-tool" data-card-widget="remove">
                                 <i class="fas fa-times"></i>
                             </button>
                         </div>
                     </div>
                     <div class="card-body">
                         <div class="chart">
                             <canvas id="donutChart2" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                         </div>
                     </div>
                     <!-- /.card-body -->
                 </div>
                 <!-- /.card -->

             </div>
             <!-- /.row -->


         </div>
         <div class="row">
             <div class="col-md-12">
                 <div class="card card-primary">
                     <div class="card-header">
                         <h3 class="card-title">Sites FCR: ¿Tu requerimiento fue solucionado en nuestro Kiosko de Autoservicio?</h3>

                         <div class="card-tools">
                             <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                 <i class="fas fa-minus"></i>
                             </button>
                             <button type="button" class="btn btn-tool" data-card-widget="remove">
                                 <i class="fas fa-times"></i>
                             </button>
                         </div>
                     </div>
                     <div class="card-body">
                         <div class="chart">
                             <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                         </div>
                     </div>
                     <!-- /.card-body -->
                 </div>
                 <!-- /.card -->
             </div>
         </div>
     </div><!-- /.container-fluid -->
 </div>
 <!-- /.content -->

 <script>
     $(document).ready(function() {
         $.ajax({
             url: "ajax/dashboard.ajax.php",
             type: "POST",
             data: {
                 'accion': 2 //Obtener datos Generales
             },
             dataType: 'json',
             success: function(respuesta) {
                 /* console.log(respuesta); */
                 $("#actualizacion").html('Última actualización: ' + respuesta[0]['actualizacionBase'].toString());
                 var promtorCant1 = respuesta[0]['SiCantQuito'].toString();
                 var promtorCant2 = respuesta[0]['SiCantGye'].toString();
                 var totalCantPro = (parseInt(respuesta[0]['SiCantQuito']) + parseInt(respuesta[0]['SiCantGye']));
                 var promtorAcc = respuesta[0]['SiAcc'].toString();

                 var detractorCant1 = respuesta[0]['NoCantQuito'].toString();
                 var detractorCant2 = respuesta[0]['NoCantGye'].toString();
                 var totalCantDet = (parseInt(respuesta[0]['NoCantQuito']) + parseInt(respuesta[0]['NoCantGye']));
                 var detractorAcc = respuesta[0]['NoAcc'].toString();

                 /* console.log(totalCantDet) */

                 //-------------
                 //- DONUT CHART -
                 //-------------
                 // Get context with jQuery - using jQuery's .get() method.
                 var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
                 var donutData = {
                     labels: [
                         'Cant',
                         'Acc',
                     ],
                     datasets: [{
                         data: [totalCantPro, promtorAcc],
                         backgroundColor: ['#f56954', '#00a65a'],
                     }]
                 }
                 var donutOptions = {
                     maintainAspectRatio: false,
                     responsive: true,
                 }
                 //Create pie or douhnut chart
                 // You can switch between pie and douhnut using the method below.
                 new Chart(donutChartCanvas, {
                     type: 'doughnut',
                     data: donutData,
                     options: donutOptions
                 })

                 //-------------
                 //- DONUT CHART -
                 //-------------
                 // Get context with jQuery - using jQuery's .get() method.
                 var donutChartCanvas = $('#donutChart2').get(0).getContext('2d')
                 var donutData = {
                     labels: [
                         'Cant',
                         'Acc',
                     ],
                     datasets: [{
                         data: [totalCantDet, detractorAcc],
                         backgroundColor: ['#f56954', '#00a65a'],
                     }]
                 }
                 var donutOptions = {
                     maintainAspectRatio: false,
                     responsive: true,
                 }
                 //Create pie or douhnut chart
                 // You can switch between pie and douhnut using the method below.
                 new Chart(donutChartCanvas, {
                     type: 'doughnut',
                     data: donutData,
                     options: donutOptions
                 })
                 //-------------
                 //- BAR CHART -
                 //-------------

                 var areaChartData2 = {
                     labels: [
                         'Cant Quito',
                         'Cant Gye',
                         'Acc',
                     ],
                     datasets: [{
                             label: 'Si',
                             backgroundColor: 'rgba(60,141,188,0.9)',
                             borderColor: 'rgba(60,141,188,0.8)',
                             pointRadius: false,
                             pointColor: '#3b8bba',
                             pointStrokeColor: 'rgba(60,141,188,1)',
                             pointHighlightFill: '#fff',
                             pointHighlightStroke: 'rgba(60,141,188,1)',
                             data: [promtorCant1, promtorCant2, promtorAcc]
                         },
                         {
                             label: 'No',
                             backgroundColor: 'rgba(210, 214, 222, 1)',
                             borderColor: 'rgba(210, 214, 222, 1)',
                             pointRadius: false,
                             pointColor: 'rgba(210, 214, 222, 1)',
                             pointStrokeColor: '#c1c7d1',
                             pointHighlightFill: '#fff',
                             pointHighlightStroke: 'rgba(220,220,220,1)',
                             data: [detractorCant1, detractorCant2, detractorAcc]
                         },
                     ]
                 }


                 var barChartCanvas = $('#barChart').get(0).getContext('2d')
                 var barChartData = $.extend(true, {}, areaChartData2)
                 var temp0 = areaChartData2.datasets[0]
                 var temp1 = areaChartData2.datasets[1]
                 barChartData.datasets[0] = temp1
                 barChartData.datasets[1] = temp0

                 var barChartOptions = {
                     responsive: true,
                     maintainAspectRatio: false,
                     datasetFill: false
                 }

                 new Chart(barChartCanvas, {
                     type: 'bar',
                     data: barChartData,
                     options: barChartOptions
                 })

             }


         });


         $.ajax({
             url: "ajax/dashboard.ajax.php",
             type: "POST",
             data: {
                 'accion': 7 //Obtener datos FCR por fecha
             },
             dataType: 'json',
             success: function(respuesta) {
                 console.log(respuesta);
                 var fecha = [];
                 var si = [];
                 var no = [];
                 var totalSi = 0;
                 var totalNo = 0;

                 for (let i = 0; i < respuesta.length; i++) {
                     fecha.push(respuesta[i]['fecha']);
                     si.push(parseFloat(respuesta[i]['Si']).toFixed(2));
                     no.push(parseFloat(respuesta[i]['N']).toFixed(2));
                     totalSi = (parseFloat(totalSi) + parseFloat(respuesta[i]['Si'])).toFixed(2);
                     totalNo = (parseFloat(totalNo) + parseFloat(respuesta[i]['N'])).toFixed(2);

                 }


                 //UPGRADE
                 var areaChartCanvas = $('#areaChart1').get(0).getContext('2d')

                 var areaChartData = {
                     labels: fecha,
                     datasets: [{
                             label: 'Si',
                             backgroundColor: 'rgba(60,141,188,0.9)',
                             borderColor: 'rgba(60,141,188,0.8)',
                             data: si
                         },
                         {
                             label: 'No',
                             backgroundColor: 'rgba(210, 214, 222,1)',
                             borderColor: 'rgba(210, 214, 222,1)',
                             data: no
                         }
                     ]
                 }

                 var areaChartOptions = {
                     maintainAspectRatio: false,
                     responsive: true,
                     legend: {
                         display: true
                     },
                     scales: {
                         xAxes: [{
                             gridLines: {
                                 display: true,
                             }
                         }],
                         yAxes: [{
                             gridLines: {
                                 display: true,
                             }
                         }]
                     }
                 }

                 // This will get the first returned node in the jQuery collection.
                 new Chart(areaChartCanvas, {
                     type: 'line',
                     data: areaChartData,
                     options: areaChartOptions
                 })

             }


         });

         
     })
 </script>