 <!-- Content Header (Page header) -->
 <div class="content-header">
     <div class="container-fluid">
         <div class="row mb-2">
             <div class="col-sm-6">
                 <h1 class="m-0">Tablero Principal</h1>
             </div><!-- /.col -->
             <div class="col-sm-6">
                 <ol class="breadcrumb float-sm-right">
                     <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                     <li class="breadcrumb-item active">Tablero Principal</li>
                 </ol>
             </div><!-- /.col -->
         </div><!-- /.row -->
     </div><!-- /.container-fluid -->
 </div>
 <!-- /.content-header -->

 <!-- Main content -->
 <div class="content">

     <div class="container-fluid">

         <!-- row Tarjetas Informativas -->
         <div class="row">



             <!-- TARJETA TOTAL COMPRAS -->
             <div class="col-lg-3">
                 <!-- small box -->
                 <div class="small-box bg-success">
                     <div class="inner">
                         <h2 id="Upgrade">No Data</h2>

                         <h5>Upgrade</h5>
                     </div>
                     <div class="icon">
                         <i class="ion ion-cash"></i>
                     </div>
                     <a style="cursor:pointer;" onclick="CargarContenido('vistas/DashboardUpDowngrade.php','content-wrapper')" class="small-box-footer">Mas Info <i class="fas fa-arrow-circle-right"></i></a>
                 </div>
             </div>

             <!-- TARJETA TOTAL GANANCIAS -->
             <div class="col-lg-3">
                 <!-- small box -->
                 <div class="small-box bg-danger">
                     <div class="inner">
                         <h2 id="Downgrade">No Data</h2>

                         <h5>Downgrade</h5>
                     </div>
                     <div class="icon">
                         <i class="ion ion-ios-pie"></i>
                     </div>
                     <a style="cursor:pointer;" onclick="CargarContenido('vistas/DashboardUpDowngrade.php','content-wrapper')" class="small-box-footer">Mas Info <i class="fas fa-arrow-circle-right"></i></a>
                 </div>
             </div>

             <div class="col-lg-3">
                 <!-- small box -->
                 <div class="small-box bg-info">
                     <div class="inner">
                         <h2 id="Nps">80%</h2>

                         <h5>NPS</h5>
                     </div>
                     <div class="icon">
                         <i class="ion ion-clipboard"></i>
                     </div>
                     <a style="cursor:pointer;" onclick="CargarContenido('vistas/DashboardNps.php','content-wrapper')" class="small-box-footer">Mas Info <i class="fas fa-arrow-circle-right"></i></a>
                 </div>
             </div>

             <!-- TARJETA PRODUCTOS POCO STOCK -->
             <div class="col-lg-3">
                 <!-- small box -->
                 <div class="small-box bg-primary">
                     <div class="inner">
                         <h2 id="Fcr">82%</h2>

                         <h5>FCR</h5>
                     </div>
                     <div class="icon">
                         <i class="ion ion-android-remove-circle"></i>
                     </div>
                     <a style="cursor:pointer;" onclick="CargarContenido('vistas/DashboardFcr.php','content-wrapper')" class="small-box-footer">Mas Info <i class="fas fa-arrow-circle-right"></i></a>
                 </div>
             </div>



         </div> <!-- ./row Tarjetas Informativas -->

         <!-- row Grafico de barras -->
         <div class="row">
             <div class="col-md-6">
                 <!-- BAR CHART -->
                 <div class="card card-success">
                     <div class="card-header">
                         <h2 class="card-title">Metas</h2>

                         <div class="card-tools">
                             <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                 <i class="fas fa-minus"></i>
                             </button>
                             <button type="button" class="btn btn-tool" data-card-widget="remove">
                                 <i class="fas fa-times"></i>
                             </button>
                         </div>
                     </div>
                     <div class="card-body">
                         <div class="chart">
                             <canvas id="barChart2" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                         </div>
                     </div>
                     <!-- /.card-body -->
                 </div>
                 <!-- /.card -->

             </div>

             <div class="col-md-6">

                 <div class="card card-info">

                     <div class="card-header">

                         <h3 class="card-title" id="title-header">Encuesta de Satisfacción NPS, últimos 28 días</h3>

                         <div class="card-tools">

                             <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                 <i class="fas fa-minus"></i>
                             </button>
                             <button type="button" class="btn btn-tool" data-card-widget="remove">
                                 <i class="fas fa-times"></i>
                             </button>

                         </div> <!-- ./ end card-tools -->

                     </div> <!-- ./ end card-header -->


                     <div class="card-body">

                         <div class="chart">

                             <canvas id="areaChart1" style="min-height: 250px; height: 300px; max-height: 350px; width: 100%;">

                             </canvas>

                         </div>

                     </div> <!-- ./ end card-body -->

                 </div>

             </div>

         </div><!-- ./row Grafico de barras -->

         <div class="row">
             <div class="col-md-6">
                 <!-- AREA CHART -->
                 <div class="card card-primary">
                     <div class="card-header">
                         <h3 class="card-title" id="title-up">Upgrade últimos 28 días</h3>

                         <div class="card-tools">
                             <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                 <i class="fas fa-minus"></i>
                             </button>
                             <button type="button" class="btn btn-tool" data-card-widget="remove">
                                 <i class="fas fa-times"></i>
                             </button>
                         </div>
                     </div>
                     <div class="card-body">
                         <div class="chart">
                             <canvas id="areaChart3" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                         </div>
                     </div>
                     <!-- /.card-body -->


                 </div>
                 <!-- /.card -->


             </div>
             <!-- /.col (LEFT) -->
             <div class="col-md-6">
                 <!-- AREA CHART -->
                 <div class="card card-danger">
                     <div class="card-header">
                         <h3 id="title-down" class="card-title">Downgrade últimos 28 días</h3>

                         <div class="card-tools">
                             <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                 <i class="fas fa-minus"></i>
                             </button>
                             <button type="button" class="btn btn-tool" data-card-widget="remove">
                                 <i class="fas fa-times"></i>
                             </button>
                         </div>
                     </div>
                     <div class="card-body">
                         <div class="chart">
                             <canvas id="areaChart4" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                         </div>
                     </div>
                     <!-- /.card-body -->


                 </div>
                 <!-- /.card -->


             </div>
             <!-- /.col (LEFT) -->
         </div>
         <div class="row">
             <div class="col-md-12">
                 <!-- AREA CHART -->
                 <div class="card card-primary">
                     <div class="card-header">
                         <h3 class="card-title">Encuesta NPS: ¿Tu requerimiento fue solucionado en nuestro Kiosko de Autoservicio?</h3>

                         <div class="card-tools">
                             <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                 <i class="fas fa-minus"></i>
                             </button>
                             <button type="button" class="btn btn-tool" data-card-widget="remove">
                                 <i class="fas fa-times"></i>
                             </button>
                         </div>
                     </div>
                     <div class="card-body">
                         <div class="chart">
                             <canvas id="areaChart5" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                         </div>
                     </div>
                     <!-- /.card-body -->
                 </div>
                 <!-- /.card -->

             </div>
             <!-- /.col (LEFT) -->


         </div>
         <!-- /.row -->


     </div><!-- /.container-fluid -->



 </div>
 <!-- /.content -->

 <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chart.js"></script> -->
 <script>
     $(document).ready(function() {
        var metaUp;
         var metaDown;
         var metaNps;
         var metaFcr;
         var upActual;
         var downActual;
         var npsActual;
         var npsActual;

         $.ajax({
             url: "ajax/dashboard.ajax.php",
             type: "POST",
             data: {
                 'accion': 9 //Obtener datos Generales
             },
             dataType: 'json',
             success: function(respuestaMetas) {
                 console.log(respuestaMetas);
                 /* $("#Upgrade").html('$ ' + parseFloat(respuesta[0]['Up']).toFixed(2));
                 $("#Downgrade").html('$ ' + parseFloat(respuesta[0]['Down']).toFixed(2));
                 $("#Nps").html(parseFloat(respuesta[0]['porcentajePro']).toFixed(2) + '%');
                 $("#Fcr").html(parseFloat(respuesta[0]['porcentajeSi']).toFixed(2) + '%'); */


                 $.ajax({
                     url: "ajax/dashboard.ajax.php",
                     type: "POST",
                     data: {
                         'accion': 2 //Obtener datos Generales
                     },
                     dataType: 'json',
                     success: function(respuesta) {
                         console.log(respuesta);
                         metaUp = respuestaMetas[0]['Upgrade'];
                         metaDown = respuestaMetas[0]['Downgrade'];
                         metaNps = respuestaMetas[0]['Nps'];
                         metaFcr = respuestaMetas[0]['Fcr'];

                         upActual = (parseFloat(parseFloat(respuesta[0]['Up'] / respuestaMetas[0]['Upgrade'])) * 100).toFixed(2);
                         downActual = (parseFloat(parseFloat(respuestaMetas[0]['Downgrade'] / respuesta[0]['Down'])) * 100).toFixed(2);
                         npsActual = (parseFloat(parseFloat(respuesta[0]['porcentajePro'] / respuestaMetas[0]['Nps'])) * 100).toFixed(2);
                         fcrActual = (parseFloat(parseFloat(respuesta[0]['porcentajeSi'] / respuestaMetas[0]['Fcr'])) * 100).toFixed(2);
                         

                         //-------------
                         //- BAR CHART PARA METAS-
                         //-------------

                         var areaChartData = {
                             labels: ["Upgrade", 'Downgrade', 'NPS', 'FCR'],
                             datasets: [{
                                     label: 'Avance',
                                     backgroundColor: ['rgba(12, 204, 134, 0.7)', 'rgba(235, 34, 82, 0.7)', 'rgba(7, 85, 200, 0.7)', 'rgba(12, 162, 204, 0.7)'],
                                     borderColor: ['rgba(12, 204, 134, 1)', 'rgba(235, 34, 82, 1)', 'rgba(7, 85, 200, 1)', 'rgba(12, 162, 204, 1)'],
                                     pointRadius: false,
                                     pointColor: '#3b8bba',
                                     pointStrokeColor: 'rgba(60,141,188,1)',
                                     pointHighlightFill: '#fff',
                                     pointHighlightStroke: 'rgba(60,141,188,1)',
                                     borderWidth: 2, // Ancho del borde
                                     data: [upActual, downActual, npsActual, fcrActual, 100],
                                     hoverOffset: 4,
                                 },
                                 {
                                     label: '',
                                     yAxisID: 'yAxis1',
                                     backgroundColor: ['rgba(12, 204, 134, 0.1)', 'rgba(235, 34, 82, 0.1)', 'rgba(7, 85, 200, 0.1)', 'rgba(12, 162, 204, 0.1)'],
                                     borderColor: ['rgba(12, 204, 134, 1)', 'rgba(235, 34, 82, 1)', 'rgba(7, 85, 200, 1)', 'rgba(12, 162, 204, 1)'],
                                     pointRadius: false,
                                     pointColor: '#3b8bba',
                                     pointStrokeColor: 'rgba(60,141,188,1)',
                                     pointHighlightFill: '#fff',
                                     pointHighlightStroke: 'rgba(60,141,188,1)',
                                     borderWidth: 2, // Ancho del borde
                                     data: [100, 100, 100, 100],
                                     hoverOffset: 4,
                                 },
                             ]
                         }

                         var barChartCanvas = $('#barChart2').get(0).getContext('2d')
                         var barChartData = $.extend(true, {}, areaChartData)

                         var barChartOptions = {
                             tooltips: {
                                 enabled: true,
                                 mode: 'single',
                                 callbacks: {
                                     label: function(tooltipItems, data) {
                                         return data.datasets[tooltipItems.datasetIndex].label + ': ' + tooltipItems.xLabel + ' %';
                                     }
                                 }
                             },
                             responsive: true,
                             maintainAspectRatio: false,
                             datasetFill: false,
                             legend: {
                                 display: false,
                                 position: 'right',
                             },
                             scales: {
                                 xAxes: [{
                                     gridLines: {
                                         display: false,
                                     },
                                     ticks: {
                                         beginAtZero: true,
                                         fontSize: 0,
                                         callback: function(value) {
                                             return value + "%"
                                         }
                                     }
                                 }],
                                 yAxes: [{
                                         gridLines: {
                                             display: false,
                                         },
                                         labels: ['$' + metaUp,'$' + metaDown, metaNps + '%', metaFcr + '%'],
                                         position: 'right'
                                     },
                                     {
                                         id: 'yAxis1',
                                         type: 'category',
                                         offset: true,
                                         gridLines: {
                                             offsetGridLines: false,
                                             display: false,


                                         }
                                     }
                                 ]
                             },

                         }

                         new Chart(barChartCanvas, {
                             type: 'horizontalBar',
                             data: barChartData,
                             options: barChartOptions
                         })



                     }


                 })


             }


         })


     


         $.ajax({
             url: "ajax/dashboard.ajax.php",
             type: "POST",
             data: {
                 'accion': 2 //Obtener datos Generales
             },
             dataType: 'json',
             success: function(respuesta) {
                 console.log(respuesta);
                 $("#Upgrade").html('$ ' + parseFloat(respuesta[0]['Up']).toFixed(2));
                 $("#Downgrade").html('$ ' + parseFloat(respuesta[0]['Down']).toFixed(2));
                 $("#Nps").html(parseFloat(respuesta[0]['porcentajePro']).toFixed(2) + '%');
                 $("#Fcr").html(parseFloat(respuesta[0]['porcentajeSi']).toFixed(2) + '%');


             }


         })



        


         $.ajax({
             url: "ajax/dashboard.ajax.php",
             type: "POST",
             data: {
                 'accion': 6 //Obtener datos Nps por fecha
             },
             dataType: 'json',
             success: function(respuesta) {
                 /* console.log(respuesta); */
                 var fecha = [];
                 var promotor = [];
                 var detractor = [];
                 var totalPromotor = 0;
                 var totalDetractor = 0;

                 for (let i = 0; i < respuesta.length; i++) {
                     fecha.push(respuesta[i]['fecha']);
                     promotor.push(parseFloat(respuesta[i]['promotor']).toFixed(2));
                     detractor.push(parseFloat(respuesta[i]['detractor']).toFixed(2));
                     totalPromotor = (parseFloat(totalPromotor) + parseFloat(respuesta[i]['promotor'])).toFixed(2);
                     totalDetractor = (parseFloat(totalDetractor) + parseFloat(respuesta[i]['detractor'])).toFixed(2);

                 }


                 //NPS
                 var areaChartCanvas = $('#areaChart1').get(0).getContext('2d')

                 var areaChartData = {
                     labels: fecha,
                     datasets: [{
                             label: 'Promotores',
                             backgroundColor: 'rgba(60,141,188,0.9)',
                             borderColor: 'rgba(60,141,188,0.8)',
                             data: promotor
                         },
                         {
                             label: 'Detractores',
                             backgroundColor: 'rgba(210, 214, 222,1)',
                             borderColor: 'rgba(210, 214, 222,1)',
                             data: detractor
                         }
                     ]
                 }

                 var areaChartOptions = {
                     maintainAspectRatio: false,
                     responsive: true,
                     legend: {
                         display: true
                     },
                     scales: {
                         xAxes: [{
                             gridLines: {
                                 display: true,
                             },
                             callback: function(value) {
                                 return value + "%"
                             }
                         }],
                         yAxes: [{
                             gridLines: {
                                 display: true,
                             }
                         }]
                     }
                 }

                 // This will get the first returned node in the jQuery collection.
                 new Chart(areaChartCanvas, {
                     type: 'line',
                     data: areaChartData,
                     options: areaChartOptions
                 })

             }


         });

         $.ajax({
             url: "ajax/dashboard.ajax.php",
             type: "POST",
             data: {
                 'accion': 1 //Listar por fecha
             },
             dataType: 'json',
             success: function(respuesta) {
                 /* console.log(respuesta[0]['fecha']); */
                 var fecha = [];
                 var up = [];
                 var down = [];

                 for (let i = 0; i < respuesta.length; i++) {
                     fecha.push(respuesta[i]['fecha']);
                     up.push(parseFloat(respuesta[i]['UP']).toFixed(2));
                     down.push(parseFloat(respuesta[i]['down']).toFixed(2));
                    
                 }
                 

                 //UPGRADE
                 var areaChartCanvas = $('#areaChart3').get(0).getContext('2d')

                 var areaChartData = {
                     labels: fecha,
                     datasets: [{
                         label: 'Upgrade',
                         backgroundColor: 'rgba(60,141,188,0.9)',
                         borderColor: 'rgba(60,141,188,0.8)',
                         data: up
                     }]
                 }

                 var areaChartOptions = {
                     tooltips: {
                         enabled: true,
                         mode: 'single',
                         callbacks: {
                             label: function(tooltipItems, data) {
                                 return data.datasets[tooltipItems.datasetIndex].label + ': ' + '$ '+ tooltipItems.yLabel;
                             }
                         }
                     },
                     maintainAspectRatio: false,
                     responsive: true,
                     legend: {
                         display: false
                     },
                     scales: {
                         xAxes: [{
                             gridLines: {
                                 display: true,
                             },
                             ticks: {
                                 callback: function(data) {
                                     return data + "%"
                                 }
                             }
                         }],
                         yAxes: [{
                             gridLines: {
                                 display: true,
                             },
                             
                         }]
                     }
                 }

                 // This will get the first returned node in the jQuery collection.
                 new Chart(areaChartCanvas, {
                     type: 'line',
                     data: areaChartData,
                     options: areaChartOptions
                 })

                 //DOWNGRADE

                 var areaChartCanvas = $('#areaChart4').get(0).getContext('2d')

                 var areaChartData = {
                     labels: fecha,
                     datasets: [{
                         label: 'Downgrade',
                         /* backgroundColor: 'rgba(240, 33, 0,0.6)', */
                         borderColor: 'rgba(240, 33, 0,0.9)',
                         data: down
                     }]
                 }

                 var areaChartOptions = {
                    tooltips: {
                         enabled: true,
                         mode: 'single',
                         callbacks: {
                             label: function(tooltipItems, data) {
                                 return data.datasets[tooltipItems.datasetIndex].label + ': ' + '$'+ tooltipItems.yLabel;
                             }
                         }
                     },
                     maintainAspectRatio: false,
                     responsive: true,
                     legend: {
                         display: false
                     },
                     scales: {
                         xAxes: [{
                             gridLines: {
                                 display: true,
                             },
                             ticks: {
                                 callback: function(data) {
                                     return data + "%"
                                 }
                             }
                         }],
                         yAxes: [{
                             gridLines: {
                                 display: true,
                             }
                         }]
                     }
                 }

                 // This will get the first returned node in the jQuery collection.
                 new Chart(areaChartCanvas, {
                     type: 'line',
                     data: areaChartData,
                     options: areaChartOptions
                 })



             }

         });

         $.ajax({
             url: "ajax/dashboard.ajax.php",
             type: "POST",
             data: {
                 'accion': 7 //Obtener datos FCR por fecha
             },
             dataType: 'json',
             success: function(respuesta) {
                 console.log(respuesta);
                 var fecha = [];
                 var si = [];
                 var no = [];
                 var totalSi = 0;
                 var totalNo = 0;

                 for (let i = 0; i < respuesta.length; i++) {
                     fecha.push(respuesta[i]['fecha']);
                     si.push(parseFloat(respuesta[i]['Si']).toFixed(2));
                     no.push(parseFloat(respuesta[i]['N']).toFixed(2));
                     totalSi = (parseFloat(totalSi) + parseFloat(respuesta[i]['Si'])).toFixed(2);
                     totalNo = (parseFloat(totalNo) + parseFloat(respuesta[i]['N'])).toFixed(2);

                 }


                 //FCR
                 var areaChartCanvas = $('#areaChart5').get(0).getContext('2d')

                 var areaChartData = {
                     labels: fecha,
                     datasets: [{
                             label: 'Si',
                             backgroundColor: 'rgba(60,141,188,0.9)',
                             borderColor: 'rgba(60,141,188,0.8)',
                             data: si
                         },
                         {
                             label: 'No',
                             backgroundColor: 'rgba(210, 214, 222,1)',
                             borderColor: 'rgba(210, 214, 222,1)',
                             data: no
                         }
                     ]
                 }

                 var areaChartOptions = {
                     maintainAspectRatio: false,
                     responsive: true,
                     legend: {
                         display: true
                     },
                     scales: {
                         xAxes: [{
                             gridLines: {
                                 display: true,
                             }
                         }],
                         yAxes: [{
                             gridLines: {
                                 display: true,
                             }
                         }]
                     }
                 }

                 // This will get the first returned node in the jQuery collection.
                 new Chart(areaChartCanvas, {
                     type: 'line',
                     data: areaChartData,
                     options: areaChartOptions
                 })

             }


         });






     })
 </script>