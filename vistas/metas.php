 <!-- Content Header (Page header) -->
 <div class="content-header">
     <div class="container-fluid">
         <div class="row mb-2">
             <div class="col-sm-6">
                 <h1 class="m-0">Actualizar Metas</h1>
             </div><!-- /.col -->
             <div class="col-sm-6">
                 <ol class="breadcrumb float-sm-right">
                     <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                     <li class="breadcrumb-item active">Actualizar Metas</li>
                 </ol>
             </div><!-- /.col -->
         </div><!-- /.row -->
     </div><!-- /.container-fluid -->
 </div>
 <!-- /.content-header -->

 <!-- Main content -->
 <div class="content">
     <div class="container-fluid">
         <div class="row">
             <form>
                 <div class="form-row">
                     <div class="col-md-6">
                         <div class="row">
                             <div class="form-group col-md-6">
                                 <label for="Upgrade">Upgrade</label>
                                 <input type="text" class="form-control" id="up" placeholder="Upgrade">
                             </div>
                             <div class="form-group col-md-6">
                                 <label for="Downgrade">Downgrade</label>
                                 <input type="text" class="form-control" id="down" placeholder="Downgrade">
                             </div>

                         </div>
                         <div class="form-row">
                             <div class="form-group col-md-6">
                                 <label for="Nps">Nps</label>
                                 <input type="text" class="form-control" id="nps" placeholder="Nps">
                             </div>
                             <div class="form-group col-md-6">
                                 <label for="Fcr">Fcr</label>
                                 <input type="text" class="form-control" id="fcr" placeholder="Fcr">
                             </div>
                         </div>
                         <div class="row">
                             <div class="form-group">
                                 <input type="button" value="Guardar" class="btn btn-primary btn-lg" style="padding-left: 2.5rem; padding-right: 2.5rem;" id="btnGuardar">
                             </div>
                         </div>


                     </div>

                     <div class="col-md-6">
                         <!-- BAR CHART -->
                         <div class="card card-success">
                             <div class="card-header">
                                 <h2 class="card-title">Metas</h2>

                                 <div class="card-tools">
                                     <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                         <i class="fas fa-minus"></i>
                                     </button>
                                     <button type="button" class="btn btn-tool" data-card-widget="remove">
                                         <i class="fas fa-times"></i>
                                     </button>
                                 </div>
                             </div>
                             <div class="card-body">
                                 <div class="chart">
                                     <canvas id="barChart2" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                                 </div>
                             </div>
                             <!-- /.card-body -->
                         </div>
                         <!-- /.card -->

                     </div>

                 </div>



             </form>
         </div>

     </div><!-- /.container-fluid -->
 </div>
 <!-- /.content -->
 <script>
     $(document).ready(function() {
         var metaUp;
         var metaDown;
         var metaNps;
         var metaFcr;
         var upActual;
         var downActual;
         var npsActual;
         var npsActual;

         $.ajax({
             url: "ajax/dashboard.ajax.php",
             type: "POST",
             data: {
                 'accion': 9 //Obtener datos metas
             },
             dataType: 'json',
             success: function(respuestaMetas) {
                 console.log(respuestaMetas);

                 $.ajax({
                     url: "ajax/dashboard.ajax.php",
                     type: "POST",
                     data: {
                         'accion': 2 //Obtener datos Generales
                     },
                     dataType: 'json',
                     success: function(respuesta) {
                         console.log(respuesta);
                         $("#up").attr('placeholder', respuestaMetas[0]['Upgrade']);
                         $("#down").attr('placeholder', respuestaMetas[0]['Downgrade']);
                         $("#nps").attr('placeholder', respuestaMetas[0]['Nps']);
                         $("#fcr").attr('placeholder', respuestaMetas[0]['Fcr']);
                         
                         metaUp = respuestaMetas[0]['Upgrade'];
                         metaDown = respuestaMetas[0]['Downgrade'];
                         metaNps = respuestaMetas[0]['Nps'];
                         metaFcr = respuestaMetas[0]['Fcr'];

                         upActual = (parseFloat(parseFloat(respuesta[0]['Up'] / respuestaMetas[0]['Upgrade'])) * 100).toFixed(2);
                         downActual = (parseFloat(parseFloat(respuestaMetas[0]['Downgrade'] / respuesta[0]['Down'])) * 100).toFixed(2);
                         npsActual = (parseFloat(parseFloat(respuesta[0]['porcentajePro'] / respuestaMetas[0]['Nps'])) * 100).toFixed(2);
                         fcrActual = (parseFloat(parseFloat(respuesta[0]['porcentajeSi'] / respuestaMetas[0]['Fcr'])) * 100).toFixed(2);
                         

                         //-------------
                         //- BAR CHART PARA METAS-
                         //-------------

                         var areaChartData = {
                             labels: ["Upgrade", 'Downgrade', 'NPS', 'FCR'],
                             datasets: [{
                                     label: 'Avance',
                                     backgroundColor: ['rgba(12, 204, 134, 0.7)', 'rgba(235, 34, 82, 0.7)', 'rgba(7, 85, 200, 0.7)', 'rgba(12, 162, 204, 0.7)'],
                                     borderColor: ['rgba(12, 204, 134, 1)', 'rgba(235, 34, 82, 1)', 'rgba(7, 85, 200, 1)', 'rgba(12, 162, 204, 1)'],
                                     pointRadius: false,
                                     pointColor: '#3b8bba',
                                     pointStrokeColor: 'rgba(60,141,188,1)',
                                     pointHighlightFill: '#fff',
                                     pointHighlightStroke: 'rgba(60,141,188,1)',
                                     borderWidth: 2, // Ancho del borde
                                     data: [upActual, downActual, npsActual, fcrActual, 100],
                                     hoverOffset: 4,
                                 },
                                 {
                                     label: '',
                                     yAxisID: 'yAxis1',
                                     backgroundColor: ['rgba(12, 204, 134, 0.1)', 'rgba(235, 34, 82, 0.1)', 'rgba(7, 85, 200, 0.1)', 'rgba(12, 162, 204, 0.1)'],
                                     borderColor: ['rgba(12, 204, 134, 1)', 'rgba(235, 34, 82, 1)', 'rgba(7, 85, 200, 1)', 'rgba(12, 162, 204, 1)'],
                                     pointRadius: false,
                                     pointColor: '#3b8bba',
                                     pointStrokeColor: 'rgba(60,141,188,1)',
                                     pointHighlightFill: '#fff',
                                     pointHighlightStroke: 'rgba(60,141,188,1)',
                                     borderWidth: 2, // Ancho del borde
                                     data: [100, 100, 100, 100],
                                     hoverOffset: 4,
                                 },
                             ]
                         }

                         var barChartCanvas = $('#barChart2').get(0).getContext('2d')
                         var barChartData = $.extend(true, {}, areaChartData)

                         var barChartOptions = {
                             tooltips: {
                                 enabled: true,
                                 mode: 'single',
                                 callbacks: {
                                     label: function(tooltipItems, data) {
                                         return data.datasets[tooltipItems.datasetIndex].label + ': ' + tooltipItems.xLabel + ' %';
                                     }
                                 }
                             },
                             responsive: true,
                             maintainAspectRatio: false,
                             datasetFill: false,
                             legend: {
                                 display: false,
                                 position: 'right',
                             },
                             scales: {
                                 xAxes: [{
                                     gridLines: {
                                         display: false,
                                     },
                                     ticks: {
                                         beginAtZero: true,
                                         fontSize: 0,
                                         callback: function(value) {
                                             return value + "%"
                                         }
                                     }
                                 }],
                                 yAxes: [{
                                         gridLines: {
                                             display: false,
                                         },
                                         labels: ['$' + metaUp,'$' + metaDown, metaNps + '%', metaFcr + '%'],
                                         position: 'right'
                                     },
                                     {
                                         id: 'yAxis1',
                                         type: 'category',
                                         offset: true,
                                         gridLines: {
                                             offsetGridLines: false,
                                             display: false,


                                         }
                                     }
                                 ]
                             },

                         }

                         new Chart(barChartCanvas, {
                             type: 'horizontalBar',
                             data: barChartData,
                             options: barChartOptions
                         })



                     }


                 })


             }


         })


     })

     $('#btnGuardar').click(function() {
         var up = $("#up").val();
         var down = $("#down").val();
         var nps = $("#nps").val();
         var fcr = $("#fcr").val();

         Swal.fire({
             title: 'Está seguro de guardar?',
             icon: 'warning',
             showCancelButton: true,
             confirmButtonColor: '#3085d6',
             cancelButtonColor: '#d33',
             confirmButtonText: 'Si, deseo guardar',
             cancelButtonText: 'Cancelar',
         }).then((result) => {
             if (result.isConfirmed) {
                 console.log(up);
                 console.log(down);
                 console.log(nps);
                 console.log(fcr);

                 var datos = new FormData();

                 datos.append("accion", 8);
                 datos.append("up", up);
                 datos.append("down", down);
                 datos.append("nps", nps);
                 datos.append("fcr", fcr);


                 $.ajax({
                     url: "ajax/dashboard.ajax.php",
                     type: "POST",
                     data: datos,
                     cache: false,
                     contentType: false,
                     processData: false,
                     dataType: 'json',
                     success: function(respuesta) {
                         console.log(respuesta);
                         if (respuesta == "ok") {
                             Swal.fire({
                                 title: 'Se actualizaron las metas correctamente',
                                 icon: 'success',
                                 confirmButtonColor: '#3085d6',
                                 confirmButtonText: 'OK',

                             })

                         } else {
                             Swal.fire({
                                 icon: 'error',
                                 title: 'No se pudo actualizar las metas'
                             });
                         }

                         location.reload();
                     }





                 });

             }
         });

     })
 </script>