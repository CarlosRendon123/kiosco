<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a style="cursor: pointer;" class="nav-link active" onclick="CargarContenido('vistas/dashboard.php','content-wrapper')">
                Tablero
            </a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('vistas/DashboardUpDowngrade.php','content-wrapper')">
                Upgrade & Downgrade</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('vistas/DashboardNps.php','content-wrapper')">
                NPS</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('vistas/DashboardFcr.php','content-wrapper')">
                FCR</a>
        </li>
    </ul>


</nav>
<!-- /.navbar -->