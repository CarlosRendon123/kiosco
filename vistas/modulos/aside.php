 <?php 
 /* session_start();
 session_destroy(); */
 /* var_dump($_SESSION); */
 ?>
 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
     <!-- Brand Logo -->
     <a href="#" class="brand-link">
         <img src="vistas/assets/dist/img/Propuesta_2.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
         <span onclick="CargarContenido('vistas/dashboard.php','content-wrapper')" class="brand-text font-weight-light">Kiosco Dashboard</span>
     </a>
     <a href="#" class="brand-link">
         <img src="vistas/assets/dist/img/user2.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
         <?php
         if (!isset($_SESSION['nombre'])) { ?>
         <span onclick="CargarContenido('vistas/login.php','content-wrapper')" class="brand-text font-weight-light">Iniciar sesión</span>
         <?php }else{?>
            <span onclick="formSubmitClick();" class="brand-text font-weight-light">Cerrar sesión</span>
         <?php }?>

     </a>
     

     <!-- Sidebar -->
     <div class="sidebar">

         <!-- Sidebar Menu -->
         <nav class="mt-2">

             <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">

                 <li class="nav-item">
                     <a style="cursor: pointer;" class="nav-link active" onclick="CargarContenido('vistas/dashboard.php','content-wrapper')">
                         <i class="nav-icon fas fa-th"></i>
                         <p>
                             Tablero Principal
                         </p>
                     </a>
                 </li>

                 <li class="nav-item">
                     <a href="#" class="nav-link">
                         <i class="nav-icon fas fa-tachometer-alt"></i>
                         <p>
                             Mantenimiento
                             <i class="right fas fa-angle-left"></i>
                         </p>
                     </a>
                     <ul class="nav nav-treeview">
                         <li class="nav-item">
                             <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('vistas/metas.php','content-wrapper')">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Metas</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('vistas/cargaMasiva.php','content-wrapper')">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Carga Masiva</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('vistas/cargaMasivaNps.php','content-wrapper')">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Cargar NPS</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('vistas/cargaMasivaFcr.php','content-wrapper')">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Cargar FCR</p>
                             </a>
                         </li>
                         <!-- <li class="nav-item">
                         <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('vistas/categorias.php','content-wrapper')">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Categorías</p>
                             </a>
                         </li> -->
                     </ul>
                 </li>
                 <li class="nav-item">
                     <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('vistas/DashboardUpDowngrade.php','content-wrapper')">
                         <i class="nav-icon fas fa-th"></i>
                         <p>
                             Upgrade & Downgrade
                         </p>
                     </a>
                 </li>
                 <li class="nav-item">
                     <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('vistas/DashboardNps.php','content-wrapper')">
                         <i class="nav-icon fas fa-th"></i>
                         <p>
                             NPS
                         </p>
                     </a>
                 </li>
                 <li class="nav-item">
                     <a style="cursor: pointer;" class="nav-link" onclick="CargarContenido('vistas/DashboardFcr.php','content-wrapper')">
                         <i class="nav-icon fas fa-th"></i>
                         <p>
                             FCR
                         </p>
                     </a>
                 </li>
             </ul>
         </nav>
         <!-- /.sidebar-menu -->
     </div>
     <!-- /.sidebar -->
 </aside>

 <script>
     $(".nav-link").on('click', function() {
         $(".nav-link").removeClass('active');
         $(this).addClass('active');
     })

     function formSubmitClick() {
        var datos = new FormData();

        datos.append("accion", 2);
        /* datos.append("user", $('#User').val()); //meta upgrade
        datos.append("pass", $('#Pass').val()); */

        /* console.log( datos.get('user') );
        console.log( datos.get('pass') ); */

        $.ajax({
            url: "ajax/login.ajax.php",
            method: "POST",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(respuesta) {
                console.log("respuesta", respuesta);
                /* if (!Array.isArray(respuesta) || respuesta.length === 0) {
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        title: 'Usuario o contraseña incorrecta, por favor intente de nuevo!',
                        showConfirmButton: false,
                        timer: 2500
                    })
                } else {  */

                    /* Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Sesión iniciada, redireccionando...',
                        showConfirmButton: false,
                        timer: 2500
                    }) */

                    /* $("." + 'content-wrapper').load('Views/pages/dashboard.php');

                    window.location.reload(); */


                /* } */

            }

        });




    }
 </script>