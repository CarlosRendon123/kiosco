 <!-- Content Header (Page header) -->
 <div class="content-header">
   <div class="container-fluid">
     <div class="row mb-2">
       <div class="col-sm-6">
         <h1 class="m-0">Upgrade & Downgrade</h1>
         <p id="actualizacion">Ultima actualización</p>
       </div><!-- /.col -->
       <div class="col-sm-6">
         <ol class="breadcrumb float-sm-right">
           <li class="breadcrumb-item"><a href="#">Inicio</a></li>
           <li class="breadcrumb-item active">Upgrade & Downgrade</li>
         </ol>
       </div><!-- /.col -->
     </div><!-- /.row -->
   </div><!-- /.container-fluid -->
 </div>
 <!-- /.content-header -->

 <!-- Main content -->
 <div class="content">
   <div class="container-fluid">


     <div class="row">
       <div class="col-md-6">
         <!-- AREA CHART -->
         <div class="card card-primary">
           <div class="card-header">
             <h3 class="card-title" id="title-up">Upgrade últimos 28 días</h3>

             <div class="card-tools">
               <button type="button" class="btn btn-tool" data-card-widget="collapse">
                 <i class="fas fa-minus"></i>
               </button>
               <button type="button" class="btn btn-tool" data-card-widget="remove">
                 <i class="fas fa-times"></i>
               </button>
             </div>
           </div>
           <div class="card-body">
             <div class="chart">
               <canvas id="areaChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
             </div>
           </div>
           <!-- /.card-body -->


         </div>
         <!-- /.card -->


       </div>
       <!-- /.col (LEFT) -->
       <div class="col-md-6">
         <!-- AREA CHART -->
         <div class="card card-danger">
           <div class="card-header">
             <h3 id="title-down" class="card-title">Downgrade últimos 28 días</h3>

             <div class="card-tools">
               <button type="button" class="btn btn-tool" data-card-widget="collapse">
                 <i class="fas fa-minus"></i>
               </button>
               <button type="button" class="btn btn-tool" data-card-widget="remove">
                 <i class="fas fa-times"></i>
               </button>
             </div>
           </div>
           <div class="card-body">
             <div class="chart">
               <canvas id="areaChart2" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
             </div>
           </div>
           <!-- /.card-body -->


         </div>
         <!-- /.card -->


       </div>
       <!-- /.col (LEFT) -->
     </div>



     <div class="row">
       <div class="col-md-6">
         <!-- LINE CHART -->
         <div class="card card-info">
           <div class="card-header">
             <h3 class="card-title">Proveedores Upgrade</h3>

             <div class="card-tools">
               <button type="button" class="btn btn-tool" data-card-widget="collapse">
                 <i class="fas fa-minus"></i>
               </button>
               <button type="button" class="btn btn-tool" data-card-widget="remove">
                 <i class="fas fa-times"></i>
               </button>
             </div>
           </div>
           <div class="card-body">
             <div class="chart">
               <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
             </div>
           </div>
           <!-- /.card-body -->
         </div>
         <!-- /.card -->


       </div>
       <!-- /.col (RIGHT) -->


       <div class="col-md-6">
         <!-- LINE CHART -->
         <div class="card card-danger">
           <div class="card-header">
             <h3 class="card-title">Proveedores Downgrade </h3>

             <div class="card-tools">
               <button type="button" class="btn btn-tool" data-card-widget="collapse">
                 <i class="fas fa-minus"></i>
               </button>
               <button type="button" class="btn btn-tool" data-card-widget="remove">
                 <i class="fas fa-times"></i>
               </button>
             </div>
           </div>
           <div class="card-body">
             <div class="chart">
               <canvas id="donutChart2" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
             </div>
           </div>
           <!-- /.card-body -->
         </div>
         <!-- /.card -->

       </div>
       <!-- /.row -->


     </div>
     <!-- /.col (RIGHT) -->

     <div class="row">
       <div class="col-md-12">
         <div class="card card-primary">
           <div class="card-header">
             <h3 class="card-title">Sites Upgrade & Downgrade</h3>

             <div class="card-tools">
               <button type="button" class="btn btn-tool" data-card-widget="collapse">
                 <i class="fas fa-minus"></i>
               </button>
               <button type="button" class="btn btn-tool" data-card-widget="remove">
                 <i class="fas fa-times"></i>
               </button>
             </div>
           </div>
           <div class="card-body">
             <div class="chart">
               <canvas id="stackedBarChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
             </div>
           </div>
           <!-- /.card-body -->
         </div>
         <!-- /.card -->
       </div>
     </div>



     <div class="row">

       <div class="col-md-12">
         <div class="card card-info">
           <div class="card-header">
             <h3 class="card-title" id="title-header">Asesores Cant Quito</h3>
             <div class="card-tools">
               <button type="button" class="btn btn-tool" data-card-widget="collapse">
                 <i class="fas fa-minus"></i>
               </button>
               <button type="button" class="btn btn-tool" data-card-widget="remove">
                 <i class="fas fa-times"></i>
               </button>
             </div> <!-- ./ end card-tools -->
           </div> <!-- ./ end card-header -->

           <div class="card-body">
             <div class="chart">
               <canvas id="barChartCant1" style="min-height: 250px; height: 300px; max-height: 350px; width: 100%;">
               </canvas>
             </div>
           </div> <!-- ./ end card-body -->

         </div>
       </div>
     </div>
     <div class="row">

       <div class="col-md-12">
         <div class="card card-primary">
           <div class="card-header">
             <h3 class="card-title" id="title-header">Asesores Cant GYE</h3>
             <div class="card-tools">
               <button type="button" class="btn btn-tool" data-card-widget="collapse">
                 <i class="fas fa-minus"></i>
               </button>
               <button type="button" class="btn btn-tool" data-card-widget="remove">
                 <i class="fas fa-times"></i>
               </button>
             </div> <!-- ./ end card-tools -->
           </div> <!-- ./ end card-header -->

           <div class="card-body">
             <div class="chart">
               <canvas id="barChartCant2" style="min-height: 250px; height: 300px; max-height: 350px; width: 100%;">
               </canvas>
             </div>
           </div> <!-- ./ end card-body -->

         </div>
       </div>
     </div>

     <div class="row">

       <div class="col-md-12">
         <div class="card card-success">
           <div class="card-header">
             <h3 class="card-title" id="title-header">Asesores ACC GYE</h3>
             <div class="card-tools">
               <button type="button" class="btn btn-tool" data-card-widget="collapse">
                 <i class="fas fa-minus"></i>
               </button>
               <button type="button" class="btn btn-tool" data-card-widget="remove">
                 <i class="fas fa-times"></i>
               </button>
             </div> <!-- ./ end card-tools -->
           </div> <!-- ./ end card-header -->

           <div class="card-body">
             <div class="chart">
               <canvas id="barChartAcc" style="min-height: 250px; height: 300px; max-height: 350px; width: 100%;">
               </canvas>
             </div>
           </div> <!-- ./ end card-body -->

         </div>
       </div>
     </div>

   </div>
 </div><!-- /.container-fluid -->
 </div>
 <!-- /.content -->

 <script>
   $(document).ready(function() {
     var totalUpCant1 = 0;
     var totalDownCant1 = 0;
     var totalUpCant2 = 0;
     var totalDownCant2 = 0;
     var totalUpAcc = 0;
     var totalDownAcc = 0;
     var totalCantUp = 0;
     var totalCantDown = 0;

     var totalUp = 0;
     var totalDown = 0;

     $.ajax({
       url: "ajax/dashboard.ajax.php",
       type: "POST",
       data: {
         'accion': 2 //Obtener datos 
       },
       dataType: 'json',
       success: function(respuesta) {
        $("#actualizacion").html('Última actualización: ' + respuesta[0]['actualizacionBase'].toString());

       }
       

       });



     $.ajax({
       url: "ajax/dashboard.ajax.php",
       type: "POST",
       data: {
         'accion': 1 //Listar por fecha
       },
       dataType: 'json',
       success: function(respuesta) {
         /* console.log(respuesta[0]['fecha']); */
         var fecha = [];
         var up = [];
         var down = [];

         for (let i = 0; i < respuesta.length; i++) {
           fecha.push(respuesta[i]['fecha']);
           up.push(parseFloat(respuesta[i]['UP']).toFixed(2));
           down.push(parseFloat(respuesta[i]['down']).toFixed(2));
           totalUp = (parseFloat(totalUp) + parseFloat(respuesta[i]['UP'])).toFixed(2);
           totalDown = (parseFloat(totalDown) + parseFloat(respuesta[i]['down'])).toFixed(2);

         }

         $("#title-up").html('Upgrade últimos 28 días: $ ' + totalUp.toString());
         $("#title-down").html('Downgrade últimos 28 días: $ ' + totalDown.toString());
         /* console.log(totalDown);
         console.log(totalUp); */

         //UPGRADE
         var areaChartCanvas = $('#areaChart').get(0).getContext('2d')

         var areaChartData = {
           labels: fecha,
           datasets: [{
             label: 'Upgrade $',
             backgroundColor: 'rgba(60,141,188,0.9)',
             borderColor: 'rgba(60,141,188,0.8)',
             data: up
           }]
         }

         var areaChartOptions = {
           maintainAspectRatio: false,
           responsive: true,
           legend: {
             display: false
           },
           scales: {
             xAxes: [{
               gridLines: {
                 display: true,
               }
             }],
             yAxes: [{
               gridLines: {
                 display: true,
               }
             }]
           }
         }

         // This will get the first returned node in the jQuery collection.
         new Chart(areaChartCanvas, {
           type: 'line',
           data: areaChartData,
           options: areaChartOptions
         })

         //DOWNGRADE

         var areaChartCanvas = $('#areaChart2').get(0).getContext('2d')

         var areaChartData = {
           labels: fecha,
           datasets: [{
             label: 'Downgrade $',
             /* backgroundColor: 'rgba(240, 33, 0,0.6)', */
             borderColor: 'rgba(240, 33, 0,0.9)',
             data: down
           }]
         }

         var areaChartOptions = {
           maintainAspectRatio: false,
           responsive: true,
           legend: {
             display: false
           },
           scales: {
             xAxes: [{
               gridLines: {
                 display: true,
               }
             }],
             yAxes: [{
               gridLines: {
                 display: true,
               }
             }]
           }
         }

         // This will get the first returned node in the jQuery collection.
         new Chart(areaChartCanvas, {
           type: 'line',
           data: areaChartData,
           options: areaChartOptions
         })



       }

     });


     /* ASESORES CANT QUITO */

     $.ajax({
       url: "ajax/dashboard.ajax.php",
       type: "POST",
       data: {
         'accion': 3 //Listar por fecha
       },
       dataType: 'json',
       success: function(respuesta) {
         /* console.log(respuesta) */
         var nombre = [];
         var upCant1 = [];
         var downCant1 = [];


         for (let i = 0; i < respuesta.length; i++) {
           nombre.push(respuesta[i]['nombre']);
           upCant1.push(parseFloat(respuesta[i]['UP']).toFixed(2));
           downCant1.push(parseFloat(respuesta[i]['down']).toFixed(2));
           totalUpCant1 = (parseFloat(totalUpCant1) + parseFloat(respuesta[i]['UP'])).toFixed(2);
           totalDownCant1 = (parseFloat(totalDownCant1) + parseFloat(respuesta[i]['down'])).toFixed(2);
           totalCantUp = (parseFloat(totalCantUp) + parseFloat(respuesta[i]['UP'])).toFixed(2);
           totalCantDown = (parseFloat(totalCantDown) + parseFloat(respuesta[i]['down'])).toFixed(2);


         }
         /* console.log(totalUpCant1); */

         var areaChartData = {
           labels: nombre,
           datasets: [{
               label: 'Upgrade  $',
               backgroundColor: 'rgba(60,141,188,0.9)',
               borderColor: 'rgba(60,141,188,0.8)',
               pointRadius: false,
               pointColor: '#3b8bba',
               pointStrokeColor: 'rgba(60,141,188,1)',
               pointHighlightFill: '#fff',
               pointHighlightStroke: 'rgba(60,141,188,1)',
               data: upCant1
             },
             {
               label: 'Downgrade  $',
               backgroundColor: 'rgba(210, 214, 222, 1)',
               borderColor: 'rgba(210, 214, 222, 1)',
               pointRadius: false,
               pointColor: 'rgba(210, 214, 222, 1)',
               pointStrokeColor: '#c1c7d1',
               pointHighlightFill: '#fff',
               pointHighlightStroke: 'rgba(220,220,220,1)',
               data: downCant1
             },
           ]
         }


         //-------------
         //- BAR CHART -
         //-------------
         var barChartCanvas = $('#barChartCant1').get(0).getContext('2d')
         var barChartData = $.extend(true, {}, areaChartData)

         var barChartOptions = {
           responsive: true,
           maintainAspectRatio: false,
           scales: {
             xAxes: [{
               stacked: true,
             }],
             yAxes: [{
               stacked: true,
               ticks: {
                         fontSize: 10,
                         beginAtZero: true
                     }
               
             }]
           }
         }

         new Chart(barChartCanvas, {
           type: 'horizontalBar',
           data: barChartData,
           options: barChartOptions
         })

       }


     });

     /* ASESORES CANT GYE */

     $.ajax({
       url: "ajax/dashboard.ajax.php",
       type: "POST",
       data: {
         'accion': 4 //Listar por ASESORES
       },
       dataType: 'json',
       success: function(respuesta) {
         /* console.log(respuesta) */
         var nombre = [];
         var upCant2 = [];
         var downCant2 = [];

         for (let i = 0; i < respuesta.length; i++) {
           nombre.push(respuesta[i]['nombre']);
           upCant2.push(parseFloat(respuesta[i]['UP']).toFixed(2));
           downCant2.push(parseFloat(respuesta[i]['down']).toFixed(2));
           totalUpCant2 = (parseFloat(totalUpCant2) + parseFloat(respuesta[i]['UP'])).toFixed(2);
           totalDownCant2 = (parseFloat(totalDownCant2) + parseFloat(respuesta[i]['down'])).toFixed(2);
           totalCantUp = (parseFloat(totalCantUp) + parseFloat(respuesta[i]['UP'])).toFixed(2);
           totalCantDown = (parseFloat(totalCantDown) + parseFloat(respuesta[i]['down'])).toFixed(2);

         }
         /* console.log(totalUpCant2) */

         var areaChartData = {
           labels: nombre,
           datasets: [{
               label: 'Upgrade  $',
               backgroundColor: 'rgba(60,141,188,0.9)',
               borderColor: 'rgba(60,141,188,0.8)',
               pointRadius: false,
               pointColor: '#3b8bba',
               pointStrokeColor: 'rgba(60,141,188,1)',
               pointHighlightFill: '#fff',
               pointHighlightStroke: 'rgba(60,141,188,1)',
               data: upCant2
             },
             {
               label: 'Downgrade  $',
               backgroundColor: 'rgba(210, 214, 222, 1)',
               borderColor: 'rgba(210, 214, 222, 1)',
               pointRadius: false,
               pointColor: 'rgba(210, 214, 222, 1)',
               pointStrokeColor: '#c1c7d1',
               pointHighlightFill: '#fff',
               pointHighlightStroke: 'rgba(220,220,220,1)',
               data: downCant2
             },
           ]
         }

         console.log(totalCantUp);
         console.log(totalCantDown);


         //-------------
         //- BAR CHART -
         //-------------
         var barChartCanvas = $('#barChartCant2').get(0).getContext('2d')
         var barChartData = $.extend(true, {}, areaChartData)

         var barChartOptions = {
           responsive: true,
           maintainAspectRatio: false,
           scales: {
             xAxes: [{
               stacked: true,
             }],
             yAxes: [{
               stacked: true,
               ticks: {
                         fontSize: 10,
                         beginAtZero: true
                     }
             }]
           }
         }

         new Chart(barChartCanvas, {
           type: 'horizontalBar',
           data: barChartData,
           options: barChartOptions
         })

       }


     });


     /* Asesores ACC */

     $.ajax({
       url: "ajax/dashboard.ajax.php",
       type: "POST",
       data: {
         'accion': 5 //Listar por ASESORES
       },
       dataType: 'json',
       success: function(respuesta) {
         /* console.log(respuesta) */
         var nombre = [];
         var up = [];
         var down = [];

         for (let i = 0; i < respuesta.length; i++) {
           nombre.push(respuesta[i]['nombre']);
           up.push(parseFloat(respuesta[i]['UP']).toFixed(2));
           down.push(parseFloat(respuesta[i]['down']).toFixed(2));
           totalUpAcc = (parseFloat(totalUpAcc) + parseFloat(respuesta[i]['UP'])).toFixed(2);
           totalDownAcc = (parseFloat(totalDownAcc) + parseFloat(respuesta[i]['down'])).toFixed(2);

         }





         var areaChartData = {
           labels: nombre,
           datasets: [{
               label: 'Upgrade  $',
               backgroundColor: 'rgba(60,141,188,0.9)',
               borderColor: 'rgba(60,141,188,0.8)',
               pointRadius: false,
               pointColor: '#3b8bba',
               pointStrokeColor: 'rgba(60,141,188,1)',
               pointHighlightFill: '#fff',
               pointHighlightStroke: 'rgba(60,141,188,1)',
               data: up
             },
             {
               label: 'Downgrade  $',
               backgroundColor: 'rgba(210, 214, 222, 1)',
               borderColor: 'rgba(210, 214, 222, 1)',
               pointRadius: false,
               pointColor: 'rgba(210, 214, 222, 1)',
               pointStrokeColor: '#c1c7d1',
               pointHighlightFill: '#fff',
               pointHighlightStroke: 'rgba(220,220,220,1)',
               data: down
             },
           ]
         }


         //-------------
         //- BAR CHART -
         //-------------
         var barChartCanvas = $('#barChartAcc').get(0).getContext('2d')
         var barChartData = $.extend(true, {}, areaChartData)

         var barChartOptions = {
           responsive: true,
           maintainAspectRatio: false,
           scales: {
             xAxes: [{
               stacked: true,
             }],
             yAxes: [{
               stacked: true,
               ticks: {
                         fontSize: 10,
                         beginAtZero: true
                     }
             }]
           }
         }

         new Chart(barChartCanvas, {
           type: 'horizontalBar',
           data: barChartData,
           options: barChartOptions
         })

         //-------------
         //- DONUT CHART -
         //-------------
         // Get context with jQuery - using jQuery's .get() method.
         var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
         var donutData = {
           labels: [
             'Cant  $',
             'Acc  $',
           ],
           datasets: [{
             data: [totalCantUp, totalUpAcc],
             backgroundColor: ['#f56954', '#00a65a'],
           }]
         }
         var donutOptions = {
           maintainAspectRatio: false,
           responsive: true,
         }
         //Create pie or douhnut chart
         // You can switch between pie and douhnut using the method below.
         new Chart(donutChartCanvas, {
           type: 'doughnut',
           data: donutData,
           options: donutOptions
         })

         //-------------
         //- DONUT CHART -
         //-------------
         // Get context with jQuery - using jQuery's .get() method.
         var donutChartCanvas = $('#donutChart2').get(0).getContext('2d')
         var donutData = {
           labels: [
             'Cant  $',
             'Acc  $',
           ],
           datasets: [{
             data: [totalCantDown, totalDownAcc],
             backgroundColor: ['#f56954', '#00a65a'],
           }]
         }
         var donutOptions = {
           maintainAspectRatio: false,
           responsive: true,
         }
         //Create pie or douhnut chart
         // You can switch between pie and douhnut using the method below.
         new Chart(donutChartCanvas, {
           type: 'doughnut',
           data: donutData,
           options: donutOptions
         })


         var barChartData2 = {
           labels: [
             'Cant Quito  $',
             'Cant Gye  $',
             'Acc  $',
           ],
           datasets: [{
               label: 'Upgrade  $',
               backgroundColor: 'rgba(60,141,188,0.9)',
               borderColor: 'rgba(60,141,188,0.8)',
               pointRadius: false,
               pointColor: '#3b8bba',
               pointStrokeColor: 'rgba(60,141,188,1)',
               pointHighlightFill: '#fff',
               pointHighlightStroke: 'rgba(60,141,188,1)',
               data: [totalUpCant1,totalUpCant2,totalUpAcc]
             },
             {
               label: 'Downgrade  $',
               backgroundColor: 'rgba(210, 214, 222, 1)',
               borderColor: 'rgba(210, 214, 222, 1)',
               pointRadius: false,
               pointColor: 'rgba(210, 214, 222, 1)',
               pointStrokeColor: '#c1c7d1',
               pointHighlightFill: '#fff',
               pointHighlightStroke: 'rgba(220,220,220,1)',
               data: [totalDownCant1,totalDownCant2,totalDownAcc]
             },
           ]
         }

         //---------------------
         //- STACKED BAR CHART -
         //---------------------
         var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
         var stackedBarChartData = $.extend(true, {}, barChartData2)

         var stackedBarChartOptions = {
           responsive: true,
           maintainAspectRatio: false,
           scales: {
             xAxes: [{
               stacked: true,
             }],
             yAxes: [{
               stacked: true
             }]
           }
         }

         new Chart(stackedBarChartCanvas, {
           type: 'bar',
           data: stackedBarChartData,
           options: stackedBarChartOptions
         })

       }


     });


   })
 </script>