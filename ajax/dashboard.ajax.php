<?php

require_once "../controladores/dashboard.controlador.php";
require_once "../modelos/dashboard.modelo.php";

class AjaxDashboard{

    public function getDatosDashboard(){

        $datos = DashboardControlador::ctrGetDatosDashboard();

        echo json_encode($datos);
    }


    public function getListarUpDown(){
        $datos = DashboardControlador::ctrGetListarUpDown();
        echo json_encode($datos);
    }

    public function getUpDownAsesoresCant1(){
        $datos = DashboardControlador::ctrUpDownAsesoresCant1();
        echo json_encode($datos);
    }
    public function getUpDownAsesoresCant2(){
        $datos = DashboardControlador::ctrUpDownAsesoresCant2();
        echo json_encode($datos);
    }
    public function getUpDownAsesoresAcc(){
        $datos = DashboardControlador::ctrUpDownAsesoresAcc();
        echo json_encode($datos);
    }
    public function getListarNps(){
        $datos = DashboardControlador::ctrGetListarNps();
        echo json_encode($datos);
    }
    public function getListarFcr(){
        $datos = DashboardControlador::ctrGetListarFcr();
        echo json_encode($datos);
    }
    public function updateMetas(){
        $datos = DashboardControlador::ctrUpdateMetas($_POST['up'],$_POST['down'],$_POST['nps'],$_POST['fcr']);
        echo json_encode($datos);
    }
    public function getDatosMetas(){
        $datos = DashboardControlador::ctrGetDatosMetas();
        echo json_encode($datos);
    }


    
   
    
  
}


if(isset($_POST['accion']) && $_POST['accion'] == 1){ //Ejecutar function Listar datos UP & Downgrade

    $datos = new AjaxDashboard();
    $datos -> getListarUpDown();

}else if(isset($_POST['accion']) && $_POST['accion'] == 2){ //Ejecutar function de Dashboard principal

    $datos = new AjaxDashboard();
    $datos -> getDatosDashboard();

}else if(isset($_POST['accion']) && $_POST['accion'] == 3){ //asesores cant QUITO

    $datos = new AjaxDashboard();
    $datos -> getUpDownAsesoresCant1();

}else if(isset($_POST['accion']) && $_POST['accion'] == 4){ //asesores cant GYE

    $datos = new AjaxDashboard();
    $datos -> getUpDownAsesoresCant2();

}else if(isset($_POST['accion']) && $_POST['accion'] == 5){ //asesores ACC

    $datos = new AjaxDashboard();
    $datos -> getUpDownAsesoresAcc();

}else if(isset($_POST['accion']) && $_POST['accion'] == 6){ //Listar NPS

    $datos = new AjaxDashboard();
    $datos -> getListarNps();

}else if(isset($_POST['accion']) && $_POST['accion'] == 7){ //Listar Fcr

    $datos = new AjaxDashboard();
    $datos -> getListarFcr();

}
else if(isset($_POST['accion']) && $_POST['accion'] == 8){ //Listar Fcr

    $datos = new AjaxDashboard();
    $datos -> updateMetas();

}else if(isset($_POST['accion']) && $_POST['accion'] == 9){ //Listar metas

    $datos = new AjaxDashboard();
    $datos -> getDatosMetas();

}
