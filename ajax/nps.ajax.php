<?php

require_once "../controladores/nps.controlador.php";
require_once "../modelos/nps.modelo.php";

require_once "../vendor/autoload.php";

class ajaxNps{

    public $file;

    public function ajaxCargaMasivaNps(){

        $respuesta = NpsControlador::ctrCargaMasivaNps($this->file);

        echo json_encode($respuesta);
    }
}

if(isset($_FILES)){
    $archivo = new ajaxNps();
    $archivo -> file = $_FILES['file'];
    $archivo -> ajaxCargaMasivaNps();
}