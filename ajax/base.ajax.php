<?php

require_once "../controladores/base.controlador.php";
require_once "../modelos/base.modelo.php";

require_once "../vendor/autoload.php";

class ajaxBase{

    public $file;

    public function ajaxCargaMasiva(){

        $respuesta = BaseControlador::ctrCargaMasiva($this->file);

        echo json_encode($respuesta);
    }
}

if(isset($_FILES)){
    $archivo = new ajaxBase();
    $archivo -> file = $_FILES['file'];
    $archivo -> ajaxCargaMasiva();
}