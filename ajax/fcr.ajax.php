<?php

require_once "../controladores/fcr.controlador.php";
require_once "../modelos/fcr.modelo.php";

require_once "../vendor/autoload.php";

class ajaxFcr{

    public $file;

    public function ajaxCargaMasivaFcr(){

        $respuesta = FcrControlador::ctrCargaMasivaFcr($this->file);

        echo json_encode($respuesta);
    }
}

if(isset($_FILES)){
    $archivo = new ajaxFcr();
    $archivo -> file = $_FILES['file'];
    $archivo -> ajaxCargaMasivaFcr();
}