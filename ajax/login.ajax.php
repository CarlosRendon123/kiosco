<?php

require_once "../controladores/login.controlador.php";
require_once "../modelos/login.modelo.php";

class ajaxLogin{

    public $user;
    public $pass;

    public function ajaxLogin(){
       
        $respuesta = LoginControlador::ctrLogin($_POST['user'],$_POST['pass']);

        echo json_encode($respuesta);
    }
    /* public function ajaxLogout(){
       
        $respuesta = LoginControlador::ctrLogout();

        echo json_encode($respuesta);
    } */
}

if(isset($_POST['accion']) && $_POST['accion'] ==1){
    $datos = new ajaxLogin();
    $datos -> ajaxLogin();
}/* else if(isset($_POST['accion']) && $_POST['accion'] ==2){
    $datos = new ajaxLogin();
    $datos -> ajaxLogout();
} */

