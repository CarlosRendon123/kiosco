<?php

class DashboardControlador{

    static public function ctrGetDatosDashboard(){

        $datos = DashboardModelo::mdlGetDatosDashboard();

        return $datos;
    }

    static public function ctrGetListarUpDown(){

        $datos = DashboardModelo::mdlGetListarUpDown();

        return $datos;
    }
    static public function ctrUpDownAsesoresCant1(){

        $datos = DashboardModelo::mdlUpDownAsesoresCant1();

        return $datos;
    }
    static public function ctrUpDownAsesoresCant2(){

        $datos = DashboardModelo::mdlUpDownAsesoresCant2();

        return $datos;
    }
    static public function ctrUpDownAsesoresAcc(){

        $datos = DashboardModelo::mdlUpDownAsesoresAcc();

        return $datos;
    }
    static public function ctrGetListarNps(){

        $datos = DashboardModelo::mdlGetListarNps();

        return $datos;
    }
    static public function ctrGetListarFcr(){

        $datos = DashboardModelo::mdlGetListarFcr();

        return $datos;
    }
    static public function ctrUpdateMetas($up,$down,$nps,$fcr){

        $datos = DashboardModelo::mdlUpdateMetas($up,$down,$nps,$fcr);

        return $datos;
    }
    static public function ctrGetDatosMetas(){

        $datos = DashboardModelo::mdlGetDatosMetas();

        return $datos;
    }

        

   
}