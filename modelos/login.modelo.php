<?php

require_once "conexion.php";


class LoginModelo
{

    static public function mdlLogin($user,$pass)
    {
        /* var_dump($user);die(); */

        $stmt = Conexion::conectar()->prepare("SELECT * FROM `usuarios` WHERE nombre = :nombre AND clave = :pass");
        $stmt->bindParam(":nombre", $user, PDO::PARAM_STR);
        $stmt->bindParam(":pass", $pass, PDO::PARAM_STR);
        $stmt->execute();

        /* return $stmt->fetchAll(); */
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    
}
