<?php

require_once "conexion.php";

use PhpOffice\PhpSpreadsheet\IOFactory;


class BaseModelo
{

    private static $SHEETNAME = 'Hoja1';

    static public function mdlCargaMasiva($file)
    {

        $nombreArchivo = $file['tmp_name'];

        $documento = IOFactory::load($nombreArchivo);

        try {
            $documento->setActiveSheetIndexByName(self::$SHEETNAME);
        } catch (\Throwable $th) {
            return null;
        }
        $hoja = $documento->getActiveSheet();

        $numeroFilas = $hoja->getHighestRow();

        $Registradas = 0;


        $stmt = Conexion::conectar()->prepare("SELECT crmone FROM `asesores`");
        $stmt->execute();
        $var[] = $stmt->fetchAll();

        /* var_dump($numeroFilas); */

        //CICLO FOR PARA REGISTROS 
        for ($i = 2; $i <= $numeroFilas; $i++) {

            $nombre = trim($hoja->getCellByColumnAndRow(6, $i)->getValue());
            
            /* var_dump($nombre); die(); */
            $res = array_search($nombre, array_column($var[0], 'crmone'), false);
            /* var_dump($res);die(); */
             
            if ($res !== false) {
                /* var_dump($nombre);
            die(); */
                $fecha = $hoja->getCellByColumnAndRow(1, $i)->getValue();
                $id_usu = BaseModelo::mdlAsesorId(trim($hoja->getCell("F" . $i)));
                $cruce = $hoja->getCellByColumnAndRow(42, $i);
                $id_pln = BaseModelo::mdlBuscarId(trim($hoja->getCell("AQ" . $i)));

                $fecha2 = date("Y-m-d", (($fecha - 25569) * 86400));
                $fecha_actualizacion = date("Y-m-d");

                /* var_dump("INSERT INTO baseplanes(fecha,
                                                                                asesor_id,
                                                                                cruce,
                                                                                plan_id,
                                                                                fecha_de_actualizacion)
                                                                    values('$fecha2',
                                                                            '$id_usu[0]',
                                                                            '$cruce',
                                                                            '$id_pln[0]',
                                                                            '$fecha_actualizacion');");
                die(); */

                if (!empty($id_usu)) {
                    $stmt = Conexion::conectar()->prepare("INSERT INTO baseplanes(fecha,
                                                                                    asesor_id,
                                                                                    cruce,
                                                                                    plan_id,
                                                                                    fecha_de_actualizacion)
                                                                        values(:fecha2,
                                                                                :id_usu,
                                                                                :cruce,
                                                                                :id_pln,
                                                                                :fecha_actualizacion);");

                    $stmt->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
                    $stmt->bindParam(":id_usu", $id_usu[0], PDO::PARAM_STR);
                    $stmt->bindParam(":cruce", $cruce, PDO::PARAM_STR);

                    $stmt->bindParam(":id_pln", $id_pln[0], PDO::PARAM_STR);
                    $stmt->bindParam(":fecha_actualizacion", $fecha_actualizacion, PDO::PARAM_STR);

                    if ($stmt->execute()) {
                        $Registradas = $Registradas + 1;
                    } else {
                        $Registradas = 0;
                    }
                }
            }


           
        }


        $respuesta["total"] = $Registradas;


        return $respuesta;
    }

    static public function mdlBuscarId($nombre)
    {

        $stmt = Conexion::conectar()->prepare("SELECT id FROM `planes` WHERE nombre = :nombre");
        $stmt->bindParam(":nombre", $nombre, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetch();
    }

    static public function mdlAsesorId($nombre)
    {

        $stmt = Conexion::conectar()->prepare("SELECT id FROM `asesores` WHERE crmone = :nombre");
        $stmt->bindParam(":nombre", $nombre, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetch();
    }
}
