<?php

require_once "conexion.php";

use PhpOffice\PhpSpreadsheet\IOFactory;


class FcrModelo
{

    static public function mdlCargaMasivaFcr($file)
    {

        $nombreArchivo = $file['tmp_name'];

        $documento = IOFactory::load($nombreArchivo);

        $hoja = $documento->getSheet(0);
        $numeroFilas = $hoja->getHighestDataRow();

        $npsRegistradas = 0;

        //CICLO FOR PARA REGISTROS 
        for ($i = 2; $i <= $numeroFilas; $i++) {

            $requerimiento = trim($hoja->getCellByColumnAndRow(24, $i));
            $cliente = trim($hoja->getCellByColumnAndRow(11, $i));
            $id_cac = FcrModelo::mdlBuscarId(trim($hoja->getCell("AQ" . $i)));
            
            $fecha = $hoja->getCellByColumnAndRow(1, $i)->getValue();

            $opcion = $hoja->getCellByColumnAndRow(23, $i);
            
            $fecha2 = date("Y-m-d",(($fecha-25569)*86400));

            $fecha_actualizacion = date("Y-m-d");


            /* var_dump("INSERT INTO fcr(requerimiento,
                                                                                cliente,
                                                                                cac_id,
                                                                                fecha,
                                                                                opcion,
                                                                                fecha_de_actualizacion)
                                                                    values('$requerimiento',
                                                                            '$cliente',
                                                                            '$id_cac[0]',
                                                                            '$fecha2',
                                                                            '$opcion',
                                                                            '$fecha_actualizacion');");
            die(); */

            if (!empty($cliente)) {
                $stmt = Conexion::conectar()->prepare("INSERT INTO fcr(requerimiento,
                                                                                cliente,
                                                                                cac_id,
                                                                                fecha,
                                                                                opcion,
                                                                                fecha_de_actualizacion)
                                                                    values(:requerimiento,
                                                                            :cliente,
                                                                            :id_cac,
                                                                            :fecha2,
                                                                            :opcion,
                                                                            :fecha_actualizacion);");

                $stmt->bindParam(":requerimiento", $requerimiento, PDO::PARAM_STR);
                $stmt->bindParam(":cliente", $cliente, PDO::PARAM_STR);
                $stmt->bindParam(":id_cac", $id_cac[0], PDO::PARAM_STR);
                
                $stmt->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
                $stmt->bindParam(":opcion", $opcion, PDO::PARAM_STR);
                $stmt->bindParam(":fecha_actualizacion", $fecha_actualizacion, PDO::PARAM_STR);

                if ($stmt->execute()) {
                    $npsRegistradas = $npsRegistradas + 1;
                } else {
                    $npsRegistradas = 0;
                }
            }
        }

       
        $respuesta["totalFcr"] = $npsRegistradas;
       

        return $respuesta;
    }

    static public function mdlBuscarId($nombreCac)
    {

        $stmt = Conexion::conectar()->prepare("SELECT id FROM `kioscos` WHERE cac = :nombreCac");
        $stmt->bindParam(":nombreCac", $nombreCac, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetch();
    }
}
