<?php

require_once "conexion.php";

class DashboardModelo
{

    static public function mdlGetDatosDashboard()
    {

        $stmt = Conexion::conectar()->prepare('call prc_ObtenerDatosDashboard()');

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    static public function mdlGetListarUpDown()
    {

        $stmt = Conexion::conectar()->prepare('call prc_ListarUpDown()');

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    static public function mdlUpDownAsesoresCant1()
    {

        $stmt = Conexion::conectar()->prepare('call prc_UpDownAsesoresCant1()');

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    static public function mdlUpDownAsesoresCant2()
    {

        $stmt = Conexion::conectar()->prepare('call prc_UpDownAsesoresCant2()');

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    static public function mdlUpDownAsesoresAcc()
    {

        $stmt = Conexion::conectar()->prepare('call prc_UpDownAsesoresAcc()');

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    static public function mdlGetListarNps()
    {

        $stmt = Conexion::conectar()->prepare('call prc_ListarNps()');

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    static public function mdlGetListarFcr()
    {

        $stmt = Conexion::conectar()->prepare('call prc_ListarFcr()');

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    static public function mdlUpdateMetas($up, $down, $nps, $fcr)
    {

       /*  var_dump("UPDATE metas SET Upgrade = '$up', Downgrade = '$down', Nps = '$nps', Fcr = '$fcr'
        WHERE id = 1;");die(); */

        $stmt = Conexion::conectar()->prepare("UPDATE metas SET Upgrade = :up, Downgrade = :down, Nps = :nps, Fcr = :fcr
        WHERE id = 1;");
       

        $stmt->bindParam(":up", $up, PDO::PARAM_STR);
        $stmt->bindParam(":down", $down, PDO::PARAM_STR);

        $stmt->bindParam(":nps", $nps, PDO::PARAM_STR);
        $stmt->bindParam(":fcr", $fcr, PDO::PARAM_STR);

        if ($stmt->execute()) {
            return "ok";
        } else {
            return Conexion::conectar()->errorInfo();
        }
    }
    static public function mdlGetDatosMetas()
    {

        $stmt = Conexion::conectar()->prepare('call prc_Listarmetas()');

        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }
}
