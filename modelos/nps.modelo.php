<?php

require_once "conexion.php";

use PhpOffice\PhpSpreadsheet\IOFactory;


class NpsModelo
{

    static public function mdlCargaMasivaNps($file)
    {

        $nombreArchivo = $file['tmp_name'];

        $documento = IOFactory::load($nombreArchivo);

        $hoja = $documento->getSheet(0);
        $numeroFilas = $hoja->getHighestDataRow();

        /*  $hojaProductos = $documento->getSheetByName("Productos");
        $numeroFilasProductos = $hojaProductos->getHighestDataRow(); */

        $npsRegistradas = 0;
        /* $productosRegistrados = 0; */

        //CICLO FOR PARA REGISTROS DE CATEGORIAS
        for ($i = 2; $i <= $numeroFilas; $i++) {

            $comentario = trim($hoja->getCellByColumnAndRow(1, $i));
            $id_cac = NpsModelo::mdlBuscarId(trim($hoja->getCell("B" . $i)));
            $contacto = trim($hoja->getCellByColumnAndRow(3, $i));
            $fecha = $hoja->getCellByColumnAndRow(4, $i)->getValue();

            $grupo = $hoja->getCellByColumnAndRow(5, $i);
            
            $fecha2 = date("Y-m-d",(($fecha-25569)*86400));

            $fecha_actualizacion = date("Y-m-d");


            /* var_dump("INSERT INTO nps(comentario,
                                                                                cac_id,
                                                                                contacto,
                                                                                fecha,
                                                                                grupo,
                                                                                fecha_de_actualizacion)
                                                                    values('$comentario',
                                                                            '$id_cac[0]',
                                                                            '$contacto',
                                                                            '$fecha2',
                                                                            '$grupo',
                                                                            '$fecha_actualizacion');");
            die(); */

            if (!empty($contacto)) {
                $stmt = Conexion::conectar()->prepare("INSERT INTO nps(comentario,
                                                                                cac_id,
                                                                                contacto,
                                                                                fecha,
                                                                                grupo,
                                                                                fecha_de_actualizacion)
                                                                    values(:comentario,
                                                                            :id_cac,
                                                                            :contacto,
                                                                            :fecha2,
                                                                            :grupo,
                                                                            :fecha_actualizacion);");

                $stmt->bindParam(":comentario", $comentario, PDO::PARAM_STR);
                $stmt->bindParam(":id_cac", $id_cac[0], PDO::PARAM_STR);
                $stmt->bindParam(":contacto", $contacto, PDO::PARAM_STR);
                $stmt->bindParam(":fecha2", $fecha2, PDO::PARAM_STR);
                $stmt->bindParam(":grupo", $grupo, PDO::PARAM_STR);
                $stmt->bindParam(":fecha_actualizacion", $fecha_actualizacion, PDO::PARAM_STR);

                if ($stmt->execute()) {
                    $npsRegistradas = $npsRegistradas + 1;
                } else {
                    $npsRegistradas = 0;
                }
            }
        }

        /* if($npsRegistradas > 0){

            //CICLO FOR PARA REGISTROS DE PRODUCTOS
            for ($i=2; $i <= $numeroFilasProductos ; $i++) { 

                $codigo_producto = $hojaProductos->getCell("A".$i);
                $id_categoria_producto = NpsModelo::mdlBuscarIdCategoria($hojaProductos->getCell("B".$i));
                $descripcion_producto = $hojaProductos->getCell("C".$i);
                $precio_compra_producto = $hojaProductos->getCell("D".$i);
                $precio_venta_producto = $hojaProductos->getCell("E".$i);
                $utilidad = $hojaProductos->getCell("F".$i);
                $stock_producto = $hojaProductos->getCell("G".$i);
                $minimo_stock_producto = $hojaProductos->getCell("H".$i);
                $ventas_producto = $hojaProductos->getCell("I".$i);
                $fecha_actualizacion_producto = date('Y-m-d');

                if(!empty($codigo_producto)){
                    $stmt = Conexion::conectar()->prepare("INSERT INTO productos(codigo_producto,
                                                                                id_categoria_producto,
                                                                                descripcion_producto,
                                                                                precio_compra_producto,
                                                                                precio_venta_producto,
                                                                                utilidad,
                                                                                stock_producto,
                                                                                minimo_stock_producto,
                                                                                ventas_producto,
                                                                                fecha_actualizacion_producto)
                                                                        values(:codigo_producto,
                                                                                :id_categoria_producto,
                                                                                :descripcion_producto,
                                                                                :precio_compra_producto,
                                                                                :precio_venta_producto,
                                                                                :utilidad,
                                                                                :stock_producto,
                                                                                :minimo_stock_producto,
                                                                                :ventas_producto,
                                                                                :fecha_actualizacion_producto);");

                    $stmt -> bindParam(":codigo_producto",$codigo_producto,PDO::PARAM_STR);
                    $stmt -> bindParam(":id_categoria_producto",$id_categoria_producto[0],PDO::PARAM_STR);
                    $stmt -> bindParam(":descripcion_producto",$descripcion_producto,PDO::PARAM_STR);
                    $stmt -> bindParam(":precio_compra_producto",$precio_compra_producto,PDO::PARAM_STR);
                    $stmt -> bindParam(":precio_venta_producto",$precio_venta_producto,PDO::PARAM_STR);
                    $stmt -> bindParam(":utilidad",$utilidad,PDO::PARAM_STR);
                    $stmt -> bindParam(":stock_producto",$stock_producto,PDO::PARAM_STR);
                    $stmt -> bindParam(":minimo_stock_producto",$minimo_stock_producto,PDO::PARAM_STR);
                    $stmt -> bindParam(":ventas_producto",$ventas_producto,PDO::PARAM_STR);
                    $stmt -> bindParam(":fecha_actualizacion_producto",$fecha_actualizacion_producto,PDO::PARAM_STR);

                    if($stmt->execute()){
                        $productosRegistrados = $productosRegistrados + 1;
                    }else{
                        $productosRegistrados = 0;
                    }
                }
            }
        } */

        $respuesta["totalNps"] = $npsRegistradas;
        /* $respuesta["totalProductos"] = $productosRegistrados; */

        return $respuesta;
    }

    static public function mdlBuscarId($nombreCac)
    {

        $stmt = Conexion::conectar()->prepare("SELECT id FROM `kioscos` WHERE cac = :nombreCac");
        $stmt->bindParam(":nombreCac", $nombreCac, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetch();
    }
}
